// Simple decorator pattern example
// (c) Daniel Livingstone 2012
// CC-BY-SA

#ifdef EXAMPLE3

#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;
using std::cin;

class AbstractNPC {
public:
	virtual ~AbstractNPC(){}
	virtual void render() = 0;
	virtual int getMana() = 0;
	virtual int getHealth() = 0;
};

class NPC: public AbstractNPC {
private:
	string name;
protected:
	int health;
	int mana;
public:
	~NPC() { cout << "Deleting NPC object " << name << endl; }
	NPC(string basename, int m, int h) { name.assign(basename); mana = m; health = h; }
	NPC(char * basename, int m, int h) { name.assign(basename); mana = m; health = h; }
	void render() { cout << name; }
	int getMana() { return mana; }
	int getHealth() { return health; }
};

class NPCDecorator: public AbstractNPC {
private:
	AbstractNPC * npc;
public:
	~NPCDecorator() { delete npc; };
	NPCDecorator(AbstractNPC *n) { npc = n; }
	void render() { npc->render(); } // delegate render to npc data member
	int getMana() { return npc->getMana(); }
	int getHealth() { return npc->getHealth(); }
};

class Elite: public NPCDecorator {
public:
	~Elite() { cout << "Deleting Elite decorator" << endl; }
	Elite(AbstractNPC *n): NPCDecorator(n) { }
	void render() {
		cout << "Elite "; // render special features
		NPCDecorator::render(); // delegate to base class
	}
	int getMana() { return NPCDecorator::getMana() + 20; }
	int getHealth() { return NPCDecorator::getHealth() + 20; }
};

class Captain: public NPCDecorator {
public:
	~Captain() { cout << "Deleting Captain decorator" << endl; } 
	Captain(AbstractNPC *n): NPCDecorator(n) { }
	void render() {
		cout << "Captain "; // render special features
		NPCDecorator::render(); // delegate to base class
	}
	int getMana() { return NPCDecorator::getMana(); }
	int getHealth() { return NPCDecorator::getHealth() + 40; }
};

class Shaman: public NPCDecorator {
public:
	~Shaman() { cout << "Deleting Shaman decorator" << endl; }
	Shaman(AbstractNPC *n): NPCDecorator(n) { }
	void render() {
		NPCDecorator::render(); // delegate to base class
		cout << " Shaman "; // render special features *after* base
	}
	int getMana() { return NPCDecorator::getMana() + 60; }
	int getHealth() { return NPCDecorator::getHealth(); }
};

int main(int argc, char **argv)
{
	AbstractNPC *goblin1= new Elite(new Shaman(new NPC("Goblin", 30, 20)));
	AbstractNPC *ogre1= new Elite(new Captain(new NPC("Ogre", 20, 60)));
	goblin1->render(); cout << endl;
	cout << "Mana: " << goblin1->getMana() << " Health: " << goblin1->getHealth() << endl;
	ogre1->render(); cout << endl;
	cout << "Mana: " << goblin1->getMana() << " Health: " << goblin1->getHealth() << endl;

	cout << endl << endl;	

	delete goblin1;
	delete ogre1;
	cin.get();
	return 0;
}

#endif