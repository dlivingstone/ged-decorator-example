// Simple decorator pattern example
// (c) Daniel Livingstone 2012
// CC-BY-SA

#ifdef EXAMPLE2

#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;
using std::cin;

class AbstractNPC {
public:
	virtual void render() = 0;
	virtual int getMana() = 0;
	virtual int getHealth() = 0;
};

class NPC: public AbstractNPC {
private:
	string name;
protected:
	int health;
	int mana;
public:
	NPC(string basename, int m, int h) { name.assign(basename); mana = m; health = h; }
	NPC(char * basename, int m, int h) { name.assign(basename); mana = m; health = h; }
	void render() { cout << name; }
	int getMana() { return mana; }
	int getHealth() { return health; }
};

class NPCDecorator: public AbstractNPC {
private:
	AbstractNPC * npc;
public:
	NPCDecorator(AbstractNPC *n) { npc = n; }
	void render() { npc->render(); } // delegate render to npc data member
	int getMana() { return npc->getMana(); }
	int getHealth() { return npc->getHealth(); }
};

class Elite: public NPCDecorator {
public:
	Elite(AbstractNPC *n): NPCDecorator(n) { }
	void render() {
		cout << "Elite "; // render special features
		NPCDecorator::render(); // delegate to base class
	}
	int getMana() { return NPCDecorator::getMana() + 20; }
	int getHealth() { return NPCDecorator::getHealth() + 20; }
};

class Captain: public NPCDecorator {
public:
	Captain(AbstractNPC *n): NPCDecorator(n) { }
	~Captain() { cout << "Deleting Captain decorator" << endl; }
	void render() {
		cout << "Captain "; // render special features
		NPCDecorator::render(); // delegate to base class
	}
	int getMana() { return NPCDecorator::getMana(); }
	int getHealth() { return NPCDecorator::getHealth() + 40; }
};

class Shaman: public NPCDecorator {
public:
	Shaman(AbstractNPC *n): NPCDecorator(n) { }
	void render() {
		NPCDecorator::render(); // delegate to base class
		cout << " Shaman "; // render special features *after* base
	}
	int getMana() { return NPCDecorator::getMana() + 60; }
	int getHealth() { return NPCDecorator::getHealth(); }
};

int main(int argc, char **argv)
{
	AbstractNPC *goblin1= new Elite(new Shaman(new NPC("Goblin", 30, 20)));
	AbstractNPC *ogre1= new Elite(new Captain(new NPC("Ogre", 20, 60)));
	goblin1->render(); cout << endl;
	cout << "Mana: " << goblin1->getMana() << " Health: " << goblin1->getHealth() << endl;
	ogre1->render(); cout << endl;
	cout << "Mana: " << goblin1->getMana() << " Health: " << goblin1->getHealth() << endl;

	// beware of the following... see below for why
	cout << endl << endl;	
	AbstractNPC *basicgoblin = new NPC("Goblin", 30, 20);
	basicgoblin->render(); cout << endl;
	cout << "Mana: " << basicgoblin->getMana() << " Health: " << basicgoblin->getHealth() << endl;
	AbstractNPC *goblinshaman = new Shaman(basicgoblin);
	goblinshaman->render(); cout << endl;
	cout << "Mana: " << goblinshaman->getMana() << " Health: " << goblinshaman->getHealth() << endl;

	delete basicgoblin;
	// uncomment the following two lines to see what happens
	// if you delete the basicgoblin and try to access the
	// goblinshaman instance...
	//goblinshaman->render(); cout << endl;
	//cout << "Mana: " << goblinshaman->getMana() << " Health: " << goblinshaman->getHealth() << endl;
	delete goblinshaman;

	delete goblin1;
	delete ogre1;
	cin.get();
	return 0;
}

#endif